userInsertSorted = ($element, $container) ->
	text = $element.find('span').text().trim()

	inserted = false

	$container.find('.list-group-item').each ->
		$current = $ @
		if text < $current.find('span').text().trim()
			inserted = true
			$element
				.hide()
				.insertBefore $current.parent()
				.slideDown()

			false

	unless inserted
		$element
			.hide()
			.appendTo $container
			.slideDown()


$ ->
	$(document).on 'click', '.js-user', (event) ->
		event.preventDefault()
		$this = $ @
		id = $this.data 'id'
		$avatar = $this.find('.avatar').clone()
		name = $this.find('span').text()

		data = user: id
		data[yii.getCsrfParam()] = yii.getCsrfToken()
		$.ajax
			url: location.pathname
			type: 'POST'
			data: data

		$element = $ '<div class="col-md-3 text-center" style="margin-bottom: 10px;"><a href="#" class="list-group-item js-user-attached"></a></div>'
		$a = $element.find('a')
		$a.data 'id', id
		$avatar.appendTo $a
		$a.append "<br><span>#{name}</span>"

		$delete = $('.js-delete').parent()
		$this.slideUp -> $this.remove()
		$delete.slideUp -> $delete.remove()

		userInsertSorted $element, $ '.js-users-attached'

	$(document).on 'click', '.js-user-attached', (event) ->
		event.preventDefault()
		$this = $ @
		$that = $this.clone()
		$that.find('br').remove()

		id = $this.data 'id'

		data =
			user: id
			del: 'y'
		data[yii.getCsrfParam()] = yii.getCsrfToken()

		$.ajax
			url: location.pathname
			type: 'POST'
			data: data

		$this.slideUp -> $this.parent().remove()
		$that
			.removeClass 'js-user-attached'
			.addClass 'js-user'
			.attr 'style', 'display: block'

		userInsertSorted $that, $ '.js-users'

	$('.js-delete').click (event) ->
		event.preventDefault()

		data = action: 'delete'
		data[yii.getCsrfParam()] = yii.getCsrfToken()

		$.ajax
			url: location.pathname
			type: 'post'
			data: data

		location.href = '/'
