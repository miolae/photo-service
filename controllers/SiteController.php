<?php

namespace app\controllers;

use app\models\Photo;
use app\models\PhotoToUser;
use app\models\User;
use yii\helpers\FileHelper;
use yii\web\Controller;

class SiteController extends Controller
{
    public function beforeAction($action)
    {
        if (\Yii::$app->request->method === 'GET') {
            $this->scan();
        }

        return parent::beforeAction($action);
    }

    private function scan()
    {
        $dir = \Yii::getAlias(\Yii::$app->params['photo_upload_dir']);
        $files = FileHelper::findFiles($dir);

        foreach ($files as $file) {
            (new Photo(['filename' => $file]))->save();
        }
    }

    public function actionIndex()
    {
        return $this->redirect('detached');
    }

    public function actionDetached()
    {
        $query = Photo::find()
            ->leftJoin(PhotoToUser::tableName(), 'id = photo_id')
            ->where('photo_id IS NULL')
            ->andWhere(['deleted' => false]);

        return $this->render('index', ['query' => $query]);
    }

    public function actionAttached()
    {
        $query = Photo::find()
            ->leftJoin(PhotoToUser::tableName(), 'id = photo_id')
            ->where('photo_id IS NOT NULL')
            ->andWhere(['deleted' => false]);

        return $this->render('index', ['query' => $query]);
    }

    public function actionDetail(int $id)
    {
        $photo = Photo::findOne($id);
        if ($photo) {
            $request = \Yii::$app->request;

            if ($request->method === 'POST') {
                if (!empty($request->post('user'))) {
                    $userId = (int)$request->post('user');

                    if ($request->post('del') === 'y') {
                        $photo->userUnlink($userId);
                    } else {
                        $photo->userLink($userId);
                    }

                } elseif ($request->post('action') === 'delete' && !$photo->deleted && empty($photo->users)) {
                    $photo->deleted = true;
                    $photo->save();
                }

                return;
            }

            $users = User::findAll();
            foreach ($photo->userIds as $id) {
                if (isset($users[ $id ])) {
                    unset($users[ $id ]);
                }
            }
            $params = [
                'model' => $photo,
                'users' => $users,
            ];

            return $this->render('detail', $params);
        } else {
            return $this->render('404');
        }
    }
}
