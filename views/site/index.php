<?php
/** @var \yii\db\ActiveQuery $query */

use app\models\Photo;

$pagination = new \yii\data\Pagination([
    'totalCount' => $query->count(),
    'forcePageParam' => false,
    'pageSizeLimit' => [1, 20],
]);

/** @var Photo[] $photos */
$photos = $query
    ->offset($pagination->offset)
    ->limit($pagination->limit)
    ->all();
?>

<div class="row">
    <?php foreach ($photos as $photo): ?>
        <div class="col-md-4" style="padding: 10px;">
            <div class="pic-box" style="padding: 10px; margin-bottom: 10px;">
                <a href="<?= \yii\helpers\Url::to(['site/detail', 'id' => $photo->id]); ?>" style="display:block;">
                    <img class="img-responsive" src="<?= $photo->link ?>" alt="<?= $photo->title ?>">
                </a>
                <div class="row">
                    <div class="col-md-12 avatar-list">
                        <? foreach ($photo->users as $user):?>
                            <img
                                    class="image-responsive"
                                    src="<?= $user->avatar ?>"
                                    alt="<?= $user->nameFull ?>"
                                    title="<?= $user->nameFull ?>"
                            >
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php

echo \yii\widgets\LinkPager::widget([
	'pagination' => $pagination,
]);
