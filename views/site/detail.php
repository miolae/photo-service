<?php
/** @var Photo $model */
/** @var User[] $users */

/** @var View[] $this */

use app\models\Photo;
use app\models\User;
use yii\helpers\Html;
use yii\web\View;

echo Html::csrfMetaTags();
?>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <img src="<?= $model->link ?>" alt="<?= $model->title ?>">
            </div>
        </div>
        <div class="row padded js-users-attached">
            <? if (empty($model->users)): ?>
                <div class="col-md-12">
                    <a href="#" class="btn btn-danger js-delete">
                        Запретить добавлять пользователей
                    </a>
                </div>
            <? endif; ?>
            <? foreach ($model->users as $user): ?>
                <div class="col-md-3 text-center" style="margin-bottom: 10px;">
                    <a href="#" class="list-group-item js-user-attached" data-id="<?= $user->id ?>">
                        <div class="avatar" style="display: inline-block; background: url('<?= $user->avatar ?>') center ; height: 50px; width: 50px; -webkit-border-radius: 50px;-moz-border-radius: 50px;border-radius: 50px; background-size: cover;"></div>
                        <br>
                        <span><?= $user->nameFull ?></span>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="list-group js-users" style="max-height: 750px; overflow: auto">
            <? foreach ($users as $user): ?>
                <div>
                    <a href="#" class="list-group-item js-user" data-id="<?= $user->id ?>">
                        <div class="avatar"
                              style="display: inline-block; background: url('<?= $user->avatar ?>') center ; height: 50px; width: 50px; -webkit-border-radius: 50px;-moz-border-radius: 50px;border-radius: 50px; background-size: cover;"></div>
                        <span><?= $user->nameFull ?></span>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
