<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo_to_user`.
 */
class m170929_105427_create_photo_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tablename = 'photo_to_user';
        $this->createTable($tablename, [
            'photo_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk', $tablename, ['photo_id', 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('photo_to_user');
    }
}
