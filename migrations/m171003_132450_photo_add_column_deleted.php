<?php

use yii\db\Migration;

class m171003_132450_photo_add_column_deleted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('photo', 'deleted', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('photo', 'deleted');
    }
}
