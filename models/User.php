<?php

namespace app\models;

use yii\base\Object;

/**
 * @property Photo[] photo
 * @property string nameFull
 */
class User extends Object
{
    /** @var User[] $cache */
    private static $cache = [];
    public $id;
    public $name;
    public $surname;
    public $avatar;

    /**
     * @param int $id
     *
     * @return User|false
     */
    public static function findOne($id)
    {
        $user = self::findAll([$id]);
        if (!empty($user)) {
            return $user[ $id ];
        }

        return false;
    }

    /**
     * @param array|null $ids
     *
     * @return  User[]
     */
    public static function findAll(array $ids = null): array
    {
        if (empty (self::$cache)) {
            self::$cache = self::sort(self::get());
        }

        if ($ids === null) {
            return self::$cache;
        }

        $result = [];
        foreach ($ids as $id) {
            if (isset(self::$cache[ $id ])) {
                $result[ $id ] = self::$cache[ $id ];
            }
        }

        return self::sort($result);
    }

    /** @return User[] */
    private static function get():array
    {
        $result = [];
        $apiParams = \Yii::$app->params['api'];
        $users = file_get_contents($apiParams['base'] . $apiParams['user-list']);
        $users = json_decode($users, true)['users'];

        foreach ($users as $user) {
            $user['id'] = (int)$user['id'];
            $user['avatar'] = ($user['avatar']['url'] ? $apiParams['base'] . $user['avatar']['url'] : '');
            $result[ $user['id'] ] = new self($user);
        }

        return $result;
    }

    /**
     * @param User[] $users
     *
     * @return User[]
     */
    private static function sort(array $users): array
    {
        $names = $result = [];

        foreach ($users as $id => $user) {
            $names[$id] = $user->nameFull;
        }

        array_multisort($names, $users);

        foreach ($users as $user) {
            $result[ $user->id ] = $user;
        }

        return $result;
    }

    public function getNameFull()
    {
        return $this->name . ' ' . $this->surname;
    }
}
