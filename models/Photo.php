<?php

namespace app\models;

use yii\base\ModelEvent;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\httpclient\Client;

/**
 * Class Photo
 * @package app\models
 *
 * @property int id
 * @property string title
 * @property string name
 * @property int created_at
 * @property int updated_at
 * @property bool deleted
 * @property User[] users
 * @property string link
 * @property int[] userIds
 */
class Photo extends ActiveRecord
{
    /** @var string */
    public $filename;

    public function init()
    {
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'fileMove']);
        parent::init();
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function fileMove(ModelEvent $event)
    {
        $mime = FileHelper::getMimeType($this->filename);
        if (!file_exists($this->filename) || strpos($mime, 'image') !== 0) {
            $event->isValid = false;

            return;
        }

        $arFile = pathinfo($this->filename);
        $this->title = $arFile['filename'];

        do {
            $this->name = md5($this->filename . microtime()) . '.' . $arFile['extension'];
            $fileTo = \Yii::getAlias(\Yii::$app->params['photo_working_dir'] . DIRECTORY_SEPARATOR . $this->name);
        } while (file_exists($fileTo));

        if (file_exists($this->filename)) {
            rename($this->filename, $fileTo);
        }
    }

    public function getUsers()
    {
        if (!$this->id) {
            return [];
        }

        $ids = $this->getUserIds();

        return User::findAll($ids);
    }

    public function getUserIds()
    {
        $ids = PhotoToUser::find()->select('user_id')->where(['photo_id' => $this->id])->column();

        return array_map('intval', $ids);
    }

    public function getLink()
    {
        $link = \Yii::$app->params['photo_working_dir'] . DIRECTORY_SEPARATOR . $this->name;

        return substr($link, strlen('@webroot'));
    }

    public function userLink(int $id)
    {
        if ($id > 0 && $user = User::findOne($id)) {
            $link = new PhotoToUser();
            $link->photo_id = $this->id;
            $link->user_id = $id;
            $result = $link->save();
            $this->sendChanges();

            return $result;
        }

        return false;
    }

    public function userUnlink(int $id)
    {
        if ($id > 0) {
            $link = PhotoToUser::findOne(['user_id' => $id, 'photo_id' => $this->id]);
            if ($link) {
                $link->delete();
            }
        }

        $this->sendChanges();
    }

    private function sendChanges()
    {
        $data = [
            'url'   => \Yii::$app->request->serverName . $this->link,
            'users' => json_encode($this->userIds),
        ];
        $apiParams = \Yii::$app->params['api'];

        $request = (new Client)->createRequest();
        $request->addHeaders(['Content-Type' => 'application/json']);
        $request->url = $apiParams['base'] . $apiParams['photo-post'];
        $request->method = 'post';
        $request->data = $data;

        echo $request->send()->content;
    }
}
